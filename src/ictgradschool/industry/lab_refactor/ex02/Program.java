package ictgradschool.industry.lab_refactor.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    protected static final Random r = new Random();
    protected Canvas c = new Canvas();
    protected int fx = -1;
    protected int fy = -1;
    protected ArrayList<Integer> xs1 = new ArrayList<>();
    protected ArrayList<Integer> xs2 = new ArrayList<>();
    protected ArrayList<Integer> ys1 = new ArrayList<>();
    protected ArrayList<Integer> ys2 = new ArrayList<>();
    protected int d;
    protected boolean done = false;

    public static void main(String[] args) {
        new Program().go();
    }
    

    public Program() {
        for (int i = 0; i < 6; i++) {
            xs2.add(10 - i);
            ys2.add(10);
        }
        this.d = 39;
        setTitle("For Goodness Snake");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        c.setBackground(Color.white);
        add(BorderLayout.CENTER, c);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int d1 = e.getKeyCode();
                if ((d1 >= 37) && (d1 <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.d - d1) != 2) {// block moving back
                        Program.this.d = d1;
                    }
                }
            }
        });
        setVisible(true);
    }



    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < xs2.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(xs2.get(i) * 25 + 1, ys2.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            g.setColor(Color.green);
            g.fill3DRect(fx * 25 + 1, fy * 25 + 1, 25 - 2, 25 - 2, true);
            for (int i = 0; i < xs1.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(xs1.get(i) * 25 + 1, ys1.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            if (done) {
                g.setColor(Color.red);
                g.setFont(new Font("Gigi", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }
}