package ictgradschool.industry.lab_refactor.ex01;

import java.io.*;
import java.util.*;

public class RandomStudentMarkGenerator {

    public static void main(String[] args) {

        String student = generateRandomStudent();
        int ranStudentSkill = (int) (Math.random() * 100) + 1;
        student = generateRandomLabScores(student, ranStudentSkill);
        student = generateRandomTestScore(student, ranStudentSkill);
        student = generateRandomExamScore(student, ranStudentSkill);
        writeStudentScoreToFile(student);
    }

    private static void writeStudentScoreToFile(String student) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
            bw.write(student + "\n");
            bw.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private static String generateRandomExamScore(String student, int ranStudentSkill) {
        if (ranStudentSkill <= 7) {
            int randDNSProb = (int) (Math.random() * 101);
            if (randDNSProb <= 5) {
                student += ""; //DNS
            } else {
                student += (int) (Math.random() * 40); //[0,39]
            }
        } else if (ranStudentSkill <= 20) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if (ranStudentSkill <= 60) {
            student += ((int) (Math.random() * 20) + 50);//[50,69]
        } else if (ranStudentSkill <= 90) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        student += "\n";
        return student;
    }
    private static String generateRandomTestScore(String student, int ranStudentSkill) {
        if (ranStudentSkill <= 5) {
            student += (int) (Math.random() * 40); //[0,39]
        } else if (ranStudentSkill <= 20) {
            student += ((int) (Math.random() * 10) + 40); //[40,49]
        } else if (ranStudentSkill <= 65) {
            student += ((int) (Math.random() * 20) + 50); //[50,69]
        } else if (ranStudentSkill <= 90) {
            student += ((int) (Math.random() * 20) + 70); //[70,89]
        } else {
            student += ((int) (Math.random() * 11) + 90); //[90,100]
        }
        student += "\t";
        return student;
    }
    private static String generateRandomLabScores(String student, int ranStudentSkill) {
        for (int j = 0; j <= 3; j++) {
            if (ranStudentSkill <= 5) {
                student += (int) (Math.random() * 40); //[0,39]
            } else if (ranStudentSkill <= 15) {
                student += ((int) (Math.random() * 10) + 40); // [40,49]
            } else if (ranStudentSkill <= 25) {
                student += ((int) (Math.random() * 20) + 50); // [50,69]
            } else if (ranStudentSkill <= 65) {
                student += ((int) (Math.random() * 20) + 70); // [70,89]
            } else {
                student += ((int) (Math.random() * 11) + 90); //[90,100]
            }
            student += "\t";
        }
        return student;
    }
    private static String generateRandomStudent() {
        ArrayList<String> firstNameList = new ArrayList<>();
        ArrayList<String> surnameList = new ArrayList<>();
        readFirstNameFromFile(firstNameList);
        readSurnameFromFile(surnameList);
        String student = ((int) (Math.random() * 550) + 1) + "";
        int randFNIndex = (int) (Math.random() * firstNameList.size());
        int randSNIndex = (int) (Math.random() * surnameList.size());
        student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
        return student;
    }
    private static void readFirstNameFromFile(ArrayList<String> firstNameList) {
        try {
            String line;
            BufferedReader br = new BufferedReader(new FileReader("FirstNames.txt"));
            while ((line = br.readLine()) != null) {
                firstNameList.add(line);
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    private static void readSurnameFromFile(ArrayList<String> surnameList) {
        try {
            BufferedReader br;
            String line;
            br = new BufferedReader(new FileReader("Surnames.txt"));
            while ((line = br.readLine()) != null) {
                surnameList.add(line);
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}